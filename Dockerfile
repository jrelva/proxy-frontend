FROM ubuntu:latest
MAINTAINER jrelva


ENV GOPATH /root


RUN apt-get update
RUN apt-get -y install nginx golang git
RUN go get github.com/benton/etcd-nginx-config

ADD vhostTemplate.go /root/src/github.com/benton/etcd-nginx-config/
RUN cd $GOPATH/src/github.com/benton/etcd-nginx-config && go build
RUN cp $GOPATH/src/github.com/benton/etcd-nginx-config/etcd-nginx-config /usr/bin/

ADD index.html /usr/share/nginx/html/
ADD start-proxy /bin/

EXPOSE 80
CMD /bin/start-proxy
